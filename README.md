[![Open Door 2020](OpenDoor.png)](#)

Source code for [ASULUG's exhibit at Open Door 2020](https://opendoor.asu.edu/tempe/taste-programming-and-embedded-systems-3549)

- [Raspberry Pi Morse Code Generator](https://gitlab.com/asulug/open-door-2020/-/tree/MorsePi)
- [Arduino LCD Printout](https://gitlab.com/asulug/open-door-2020/-/tree/ArduinoLCD)
- [Arduino Distance Detector](https://gitlab.com/asulug/open-door-2020/-/tree/ArduinoDistance)